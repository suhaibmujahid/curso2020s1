# Introduccción a los Sistemas Operativos

## 2020 Semestre 1

En este respositorio se irá publicando material de la materia (slides, código, tutoriales, etc).


## Listas

- tpi-est-so@listas.unq.edu.ar
        Dudas, consultas, avisos para comunicación con todos los estudiantes y docentes.

- tpi-doc-so@listas.unq.edu.ar
 Cualquier duda de la materia que no es para todos los compañeros, envíenla a esta lista

    Hasta que se habilite la lista  enviar el mail con copia a:  jtondato@gmail.com, fernando.garcia@unq.edu.ar, matiasmpereira@gmail.com 


## Libros usados en la cursada

- Modern Operating Systems (Tanenbaum)
- Operating System Concepts (Silberschatz)


## Slides de las Clases
<!--
- [0 - Curso](./slides/00_curso.pdf)
- [1 - Intro](./slides/01_intro.pdf)
-->

## Trabajos Prácticos

Durante la materia iremos trabajando sobre un simulador básico de un sistema operativo que nos permita entender los conceptos que iremos desarrollando a lo largo de las clases. Como hemos mencionado, a esta altura carecemos de las herramientas para trabajar sobre un sistema operativo, aún de propósito didáctico como por ejemplo NachOS. Por tal motivo elegimos trabajar con un simulador que nos permita enfocarnos en los conceptos estudiados.

- [Intro a Python](./python/python_intro.md)

Breve (muy breve) introducción a Python y al trabajo práctico.

- [Ejemplos](./python/examples)

Algunos ejemplos de Python utilizados en la Introducción.



### Git

- [Git tutorial](http://rogerdudler.github.io/git-guide/)





## Slides de Prácticas
- [Curso](./practicas/slides/00_curso.pdf)
- [Git](./practicas/slides/00_git.pdf)
- [practica 1](./practicas/slides/practica1.pdf)


## Prácticas
- [Práctica 1 - Un Simulador "Extremadamente" Simplificado.](./practicas/practica_1) 
<!---
- [Práctica 2 - Procesos - Clock - Interrupcion #KILL ](./practicas/practica_2) 
- [Práctica 3 - Multiprogramación ](./practicas/practica_3) 
- [Práctica 4 - Scheduler](./practicas/practica_4) 
- [Práctica 5 - Memoria: Paginación](./practicas/practica_5) 
- [Práctica 6 - Memoria: Paginación Bajo Demanda](./practicas/practica_6) 
-->


## Fechas de Entrega
<!--

|  Fecha | Practica   | Path                     |
|  ---   | ---------- | ---------                |
|        | Práctica 1 |  [REPO_GRUPO]/practica_1 |


-->


